// swift-tools-version:5.3

import PackageDescription

let package = Package(
	name: "RFSyntaxHighlightableTextView",
	platforms: [.macOS(.v10_11)],
	products: [
		.library(
			name: "RFSyntaxHighlightableTextView",
			targets: ["RFSyntaxHighlightableTextView"]),
	],
	dependencies: [
	],
	targets: [
		.target(
			name: "RFSyntaxHighlightableTextView",
			dependencies: [])
	]
)
