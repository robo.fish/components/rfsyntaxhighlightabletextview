//  Copyright 2020 Robo.Fish UG
#if os(macOS)

import SwiftUI
import AppKit

public struct RFSyntaxHighlightableTextView : NSViewRepresentable
{
	private let coordinator = Coordinator()

	public var text : String
	{
		set { coordinator.text = newValue }
		get { coordinator.text }
	}

	public var syntaxHighlighter : (AnyObject & RFSyntaxHighlighting)?
	{
		set { coordinator.syntaxHighlighter = newValue }
		get { coordinator.syntaxHighlighter }
	}

	public func makeNSView(context: Context) -> NSTextView
	{
		context.coordinator.textView
	}

	public func updateNSView(_ nsView: NSTextView, context: Context)
	{

	}

	public static func dismantleNSView(_ nsView: NSTextView, coordinator: Coordinator)
	{

	}

	public func makeCoordinator() -> Coordinator
	{
		self.coordinator
	}

	public class Coordinator
	{
		let textView = NSTextView(frame: .zero)

		var syntaxHighlighter : (AnyObject & RFSyntaxHighlighting)?

		var text : String
		{
			get { textView.textStorage?.string ?? "" }
			set
			{
				guard
					let textStorage = textView.textStorage,
					let highlighter = syntaxHighlighter
					else { return }
				textStorage.beginEditing()
				defer { textStorage.endEditing() }
				let fullRange = NSRange(location: 0, length: textStorage.string.count)
				textStorage.replaceCharacters(in: fullRange, with: highlighter.attributedString(for: newValue))
			}
		}

	}
}

#endif // os(macOS)
