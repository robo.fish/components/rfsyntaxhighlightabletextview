//  Copyright 2020 Robo.Fish UG

import Foundation

// Start with syntax highlighting implementation from Volta

public protocol RFSyntaxHighlighting
{
	func attributedString(for text : String) -> NSAttributedString
}
